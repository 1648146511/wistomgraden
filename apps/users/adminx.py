import xadmin

from .models import OperationFunctionTable,FileTable,MenuTable,OperationLog,PermissionFileAssociationTable,PermissionMenuAssociationtTable\
    ,PermissionOperationAssociationtable,PageElementTable,PermissionsPageElementAssociatestable,PermissionTable,RolePermissionAssociationTable\
    ,RoleTable,UserGroupPermissionsAssociationTable,UserGroupsAndUserAssociAtiontable,UserGroupTable,UserRoleAssociationTable\
    ,UserGroupRoleAssociationTable,UsersTable


class OperationFunctionTableAdmin(object):
    list_display = ['operation_id', 'operation_name', 'operation_code', 'intercept_url_prefix', 'parent_operation_id']
    search_fields = ['operation_id', 'operation_name', 'operation_code', 'intercept_url_prefix', 'parent_operation_id']
    list_filter = ['operation_id', 'operation_name', 'operation_code', 'intercept_url_prefix', 'parent_operation_id']

class  FileTableAdmin(object):
    list_display = ['file_id', 'file_name', 'file_path']
    search_fields = ['file_id', 'file_name', 'file_path']
    list_filter = ['file_id', 'file_name', 'file_path']

class MenuTableAdmin(object):
    list_display = ['menu_id', 'menu_name', 'menu_url', 'parent_menu']
    search_fields = ['menu_id', 'menu_name', 'menu_url', 'parent_menu']
    list_filter = ['menu_id', 'menu_name', 'menu_url', 'parent_menu']

class OperationLogAdmin(object):
    list_display = ['operation_id', 'operation_type', 'operation_content', 'operator', 'operation_time']
    search_fields = ['operation_id', 'operation_type', 'operation_content', 'operator', 'operation_time']
    list_filter = ['operation_id', 'operation_type', 'operation_content', 'operator', 'operation_time']

class PermissionFileAssociationTableAdmin(object):
    list_display = ['permission_id', 'file_id']
    search_fields = ['permission_id', 'file_id']
    list_filter = ['permission_id', 'file_id']

class PermissionMenuAssociationtTableAdmin(object):
    list_display = ['permission_ID', 'menu_ID']
    search_fields = ['permission_ID', 'menu_ID']
    list_filter = ['permission_ID', 'menu_ID']

class PermissionOperationAssociationtableAdmin(object):
    list_display = ['permission_ID', 'operation_ID']
    search_fields = ['permission_ID', 'operation_ID']
    list_filter = ['permission_ID', 'operation_ID']

class PageElementTableAdmin(object):
    list_display = ['page_element_ID', 'page_element_code']
    search_fields = ['page_element_ID', 'page_element_code']
    list_filter = ['page_element_ID', 'page_element_code']

class PermissionsPageElementAssociatestableAdmin(object):
    list_display = ['permission_id', 'page_element_id']
    search_fields = ['permission_id', 'page_element_id']
    list_filter = ['permission_id', 'page_element_id']

class PermissionTableAdmin(object):
    list_display = ['permission_id', 'permission_type']
    search_fields = ['permission_id', 'permission_type']
    list_filter = ['permission_id', 'permission_type']

class RolePermissionAssociationTableAdmin(object):
    list_display = ['role_ID', 'permission_ID']
    search_fields = ['role_ID', 'permission_ID']
    list_filter = ['role_ID', 'permission_ID']

class RoleTableAdmin(object):
    list_display = ['role_id', 'role_name', 'create_time']
    search_fields = ['role_id', 'role_name', 'create_time']
    list_filter = ['role_id', 'role_name', 'create_time']

class UserGroupPermissionsAssociationTableAdmin(object):
    list_display = ['user_group_id', 'permission_id']
    search_fields = ['user_group_id', 'permission_id']
    list_filter = ['user_group_id', 'permission_id']

class UserGroupsAndUserAssociAtiontableAdmin(object):
    list_display = ['user_group_ID', 'user_ID']
    search_fields = ['user_group_ID', 'user_ID']
    list_filter = ['user_group_ID', 'user_ID']

class UserGroupTableAdmin(object):
    list_display = ['user_group_id', 'user_group_name', 'create_time']
    search_fields = ['user_group_id', 'user_group_name', 'create_time']
    list_filter = ['user_group_id', 'user_group_name', 'create_time']

class UserRoleAssociationTableAdmin(object):
    list_display = ['user_ID', 'role_ID']
    search_fields = ['user_ID', 'role_ID']
    list_filter = ['user_ID', 'role_ID']

class UserGroupRoleAssociationTableAdmin(object):
    list_display = ['user_group_ID', 'role_ID']
    search_fields = ['user_group_ID', 'role_ID']
    list_filter = ['user_group_ID', 'role_ID']

class UsersTableAdmin(object):
    list_display = ['user_id', 'user_name', 'user_pw', 'phone', 'email','create_time']
    search_fields = ['user_id', 'user_name', 'user_pw', 'phone', 'email','create_time']
    list_filter = ['user_id', 'user_name', 'user_pw', 'phone', 'email','create_time']


xadmin.site.register(OperationFunctionTable, OperationFunctionTableAdmin)
xadmin.site.register(FileTable,FileTableAdmin)
xadmin.site.register(MenuTable,MenuTableAdmin)
xadmin.site.register(OperationLog,OperationLogAdmin)
xadmin.site.register(PermissionFileAssociationTable,PermissionFileAssociationTableAdmin)
xadmin.site.register(PermissionMenuAssociationtTable,PermissionMenuAssociationtTableAdmin)
xadmin.site.register(PermissionOperationAssociationtable,PermissionOperationAssociationtableAdmin)
xadmin.site.register(PageElementTable,PageElementTableAdmin)
xadmin.site.register(PermissionsPageElementAssociatestable,PermissionsPageElementAssociatestableAdmin)
xadmin.site.register(PermissionTable,PermissionTableAdmin)
xadmin.site.register(RolePermissionAssociationTable,RolePermissionAssociationTableAdmin)
xadmin.site.register(RoleTable,RoleTableAdmin)
xadmin.site.register(UserGroupPermissionsAssociationTable,UserGroupPermissionsAssociationTableAdmin)
xadmin.site.register(UserGroupsAndUserAssociAtiontable,UserGroupsAndUserAssociAtiontableAdmin)
xadmin.site.register(UserGroupTable,UserGroupTableAdmin)
xadmin.site.register(UserRoleAssociationTable,UserRoleAssociationTableAdmin)
xadmin.site.register(UserGroupRoleAssociationTable,UserGroupRoleAssociationTableAdmin)
xadmin.site.register(UsersTable,UsersTableAdmin)