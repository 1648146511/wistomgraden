from django.db import models


class OperationFunctionTable(models.Model):
    operation_id = models.IntegerField(verbose_name=u"操作id", primary_key=True)
    operation_name = models.CharField(verbose_name=u"操作名称",max_length=50, blank=True, null=True)
    operation_code = models.CharField(verbose_name=u"操作编码",max_length=50)
    intercept_url_prefix = models.CharField(verbose_name=u"拦截URL前缀", max_length=80, blank=True, null=True)
    parent_operation_id = models.IntegerField(verbose_name=u"父操作ID", blank=True, null=True)

    class Meta:
        verbose_name = u"操作功能表"
        verbose_name_plural = verbose_name

class FileTable(models.Model):
    file_id = models.IntegerField(verbose_name=u"文件ID", primary_key=True)  # Field name made lowercase.
    file_name = models.CharField(verbose_name=u"文件名称",max_length=50, blank=True, null=True)
    file_path = models.CharField(verbose_name=u"文件路径", max_length=200,blank=True, null=True)

    class Meta:
        verbose_name = u"文件表"
        verbose_name_plural = verbose_name



class MenuTable(models.Model):
    menu_id = models.IntegerField(verbose_name=u"菜单ID", primary_key=True)  # Field name made lowercase.
    menu_name = models.CharField(verbose_name=u"菜单名称",max_length=30, blank=True, null=True)
    menu_url = models.CharField(verbose_name=u"菜单URL", max_length=80, blank=True, null=True)  # Field name made lowercase.
    parent_menu = models.IntegerField(verbose_name=u"父菜单",blank=True, null=True)

    class Meta:
        verbose_name = u"菜单表"
        verbose_name_plural = verbose_name

class OperationLog(models.Model):
    operation_id = models.IntegerField(verbose_name=u"操作ID", primary_key=True)  # Field name made lowercase.
    operation_type = models.CharField(verbose_name=u"操作类型",max_length=30, blank=True, null=True)
    operation_content = models.CharField(verbose_name=u"操作内容",max_length=200, blank=True, null=True)
    operator = models.CharField(verbose_name=u"操作人",max_length=50, blank=True, null=True)
    operation_time = models.DateTimeField(verbose_name=u"操作时间",blank=True, null=True)

    class Meta:
        verbose_name = u"操作日志"
        verbose_name_plural = verbose_name


class PermissionFileAssociationTable(models.Model):
    permission_id = models.IntegerField(verbose_name=u"权限ID", blank=True, null=True)  # Field name made lowercase.
    file_id = models.IntegerField(verbose_name=u"文件ID", blank=True, null=True)  # Field name made lowercase.



    class Meta:
        verbose_name = u"权限文件关联表"
        verbose_name_plural = verbose_name


class PermissionMenuAssociationtTable(models.Model):
    permission_ID = models.ForeignKey('PermissionTable', models.DO_NOTHING, verbose_name=u"权限ID", blank=True, null=True)  # Field name made lowercase.
    menu_ID = models.ForeignKey('MenuTable', models.DO_NOTHING, verbose_name=u"菜单ID", blank=True, null=True)  # Field name made lowercase.

    class Meta:
        verbose_name = u"权限菜单关联表"
        verbose_name_plural = verbose_name


class PermissionOperationAssociationtable(models.Model):
    permission_ID = models.ForeignKey('PermissionTable', models.DO_NOTHING, verbose_name=u"权限ID", blank=True, null=True)  # Field name made lowercase.
    operation_ID= models.ForeignKey('OperationFunctionTable', models.DO_NOTHING, verbose_name=u"操作ID", blank=True, null=True)  # Field name made lowercase.

    class Meta:
        verbose_name = u"权限操作关联表"
        verbose_name_plural = verbose_name

class PageElementTable(models.Model):
    page_element_ID = models.IntegerField(verbose_name=u"页面元素ID", primary_key=True)  # Field name made lowercase.
    page_element_code = models.CharField(verbose_name=u"页面元素编码",max_length=255)

    class Meta:
        verbose_name = u"页面元素表"
        verbose_name_plural = verbose_name

class PermissionsPageElementAssociatestable(models.Model):
    permission_id = models.ForeignKey('PermissionTable', models.DO_NOTHING, verbose_name=u"权限ID", blank=True, null=True)  # Field name made lowercase.
    page_element_id = models.ForeignKey('PageElementTable', models.DO_NOTHING, verbose_name=u"页面元素ID", blank=True, null=True)  # Field name made lowercase.

    class Meta:
        verbose_name = u"权限页面元素关联表"
        verbose_name_plural = verbose_name


class PermissionTable(models.Model):
    permission_id = models.IntegerField(verbose_name=u"权限ID", primary_key=True)  # Field name made lowercase.
    permission_type = models.CharField(verbose_name=u"权限类型",max_length=50, blank=True, null=True)

    class Meta:
        verbose_name = u"权限表"
        verbose_name_plural = verbose_name

class RolePermissionAssociationTable(models.Model):
    role_ID = models.ForeignKey('RoleTable', models.DO_NOTHING, verbose_name=u"角色ID", blank=True, null=True)  # Field name made lowercase.
    permission_ID = models.ForeignKey('PermissionTable', models.DO_NOTHING, verbose_name=u"权限ID", blank=True, null=True)  # Field name made lowercase.

    class Meta:
        verbose_name = u"角色权限关联表"
        verbose_name_plural = verbose_name


class RoleTable(models.Model):
    role_id = models.IntegerField(verbose_name=u"角色ID", primary_key=True)  # Field name made lowercase.
    role_name = models.CharField(verbose_name=u"角色名称",max_length=50)
    create_time = models.DateTimeField(verbose_name=u"创建时间")

    class Meta:
        verbose_name = u"角色表"
        verbose_name_plural = verbose_name

class UserGroupPermissionsAssociationTable(models.Model):
    user_group_id = models.ForeignKey('UserGroupTable', models.DO_NOTHING, verbose_name=u"用户组ID", blank=True, null=True)  # Field name made lowercase.
    permission_id = models.ForeignKey('PermissionTable', models.DO_NOTHING, verbose_name=u"权限ID", blank=True, null=True)  # Field name made lowercase.

    class Meta:
        verbose_name = u"用户组权限关联表"
        verbose_name_plural = verbose_name

class UserGroupsAndUserAssociAtiontable(models.Model):
    user_group_ID = models.ForeignKey('UserGroupTable', models.DO_NOTHING, verbose_name=u"用户组ID", blank=True, null=True)  # Field name made lowercase.
    user_ID = models.ForeignKey('UsersTable', models.DO_NOTHING, verbose_name=u"用户ID", blank=True, null=True)  # Field name made lowercase.

    class Meta:
        verbose_name = u"用户组用户关联表"
        verbose_name_plural = verbose_name


class UserGroupTable(models.Model):
    user_group_id = models.IntegerField(verbose_name=u"用户组ID", primary_key=True)  # Field name made lowercase.
    user_group_name = models.CharField(verbose_name=u"用户组名称",max_length=50)
    create_time = models.DateTimeField(verbose_name=u"创建时间",blank=True, null=True)

    class Meta:
        verbose_name = u"用户组表"
        verbose_name_plural = verbose_name


class UserRoleAssociationTable(models.Model):
    user_ID = models.ForeignKey('UsersTable', models.DO_NOTHING, verbose_name=u"用户ID", blank=True, null=True)  # Field name made lowercase.
    role_ID = models.ForeignKey('RoleTable', models.DO_NOTHING, verbose_name=u"角色ID", blank=True, null=True)  # Field name made lowercase.

    class Meta:
        verbose_name = u"用户角色关联表"
        verbose_name_plural = verbose_name

class UserGroupRoleAssociationTable(models.Model):
    user_group_ID = models.ForeignKey('UserGroupTable', models.DO_NOTHING, verbose_name=u"用户组ID", blank=True, null=True)  # Field name made lowercase.
    role_ID = models.ForeignKey('RoleTable', models.DO_NOTHING, verbose_name=u"角色ID", blank=True, null=True)  # Field name made lowercase.

    class Meta:
        verbose_name = u"用户角色关联表"
        verbose_name_plural = verbose_name

class UsersTable(models.Model):
    user_id = models.IntegerField(verbose_name=u"角色ID", primary_key=True)  # Field name made lowercase.
    user_name = models.CharField(verbose_name=u"角色名称",max_length=255)
    user_pw = models.IntegerField(verbose_name=u"用户密码")
    phone = models.CharField(verbose_name=u"电话",max_length=20)
    email = models.CharField(verbose_name=u"邮箱",max_length=20, blank=True, null=True)
    create_time = models.DateTimeField(verbose_name=u"创建时间")

    class Meta:
        verbose_name = u"用户表"
        verbose_name_plural = verbose_name

